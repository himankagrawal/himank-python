# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.
class BaseCalculator:
    def add(self, a, b):
        return a+b
    
    def subtract(self, a, b):
        return a-b
        
    def multiply(self, a, b):
        return a*b

    def divide(self, a, b):
        return a/b
		
		
class ScientificCalculator(BaseCalculator):
    def sin(self,a):
       import math
       return math.sin(math.radians(a))
	   
    def cos(self,a):
       import math
       return math.cos(math.radians(a))

    def tan(self,a):
       import math
       return math.tan(math.radians(a))	   

#create a calculator object
my_cl = ScientificCalculator()

while True:

    print("1: Add")
    print("2: Subtract")
    print("3: Multiply")
    print("4: Divide")
    print("5: Sine of angle")
    print("6: Cosine of angle")
    print("7: Tan of angle")
    print("8: Exit")
    
    ch = int(input("Select operation: "))
    
    #Make sure the user have entered the valid choice
    if ch in (1, 2, 3, 4, 5, 6, 7, 8):
        
        #first check whether user want to exit
        if(ch == 8):
            break
        
        #If not then ask fo the input and call appropiate methods   
        
        
        if(ch == 1):
            a = int(input("Enter first number: "))
            b = int(input("Enter second number: "))
            print(a, "+", b, "=", my_cl.add(a, b))
        elif(ch == 2):
            a = int(input("Enter first number: "))
            b = int(input("Enter second number: "))
            print(a, "-", b, "=", my_cl.subtract(a, b))
        elif(ch == 3):
            a = int(input("Enter first number: "))
            b = int(input("Enter second number: "))
            print(a, "*", b, "=", my_cl.multiply(a, b))
        elif(ch == 4):
            a = int(input("Enter first number: "))
            b = int(input("Enter second number: "))
            print(a, "/", b, "=", my_cl.divide(a, b))
        elif(ch == 5):
            a = int(input("Enter the angle for value of sine function "))
            print("sin(", a, ") = ", my_cl.sin(a))
        elif(ch == 6):
            a = int(input("Enter the angle for value of cos function "))
            print("cos(", a, ") = ", my_cl.cos(a))
        elif(ch == 7):
            a = int(input("Enter the angle for value of tan function"))
            print("tan (", a, ") = ", my_cl.tan(a))
    
    else:
        print("Invalid Input")